package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	// os.Args provides access to raw command-line arguments
	argv := os.Args

	// if no argument is provided, quit
	if len(os.Args) < 2 {
		fmt.Println("Usage: " + argv[0] + " string")
		return
	}

	// store in a var what the string the user want to search
	arg := argv[1]

	// if the user ask for help, tell them to go check the README!
	if arg == "help" || arg == "?" {
		fmt.Println("[-] Check the README!")
		return
	}

	// Open a file and use a second var to check for err
	f, err := os.Open("France.txt")

	// Check for file opening error
	if err != nil {
		fmt.Println("[-] File error")
		fmt.Println(err)
		return
	}

	// If the file can be opened it will tell exactly that to the user!
	fmt.Println("[+] File opened!")

	// Use defer to close the file when function return
	defer f.Close()

	// Splits on newlines by default.
	scanner := bufio.NewScanner(f)

	// Initiat var line by 1
	line := 1

	// Bool expression set to false until we found something
	found := false

	// https://golang.org/pkg/bufio/#Scanner.Scan
	for scanner.Scan() {
		// If we find something execute the code below
		if strings.Contains(scanner.Text(), arg) {
			// Make our boolean value true
			found = true

			// Tell the user we found something
			fmt.Println("[+] Found something!")

			// Convert and make the data human readable
			lineStr := scanner.Text()
			num, _ := strconv.Atoi(lineStr)

			// Print the data found to the user
			fmt.Println(lineStr, num)
		}
		// Increment at each line we go through
		line++
	}

	// If no data has been found tell the user and quit
	if !found {
		fmt.Println("[-] Didn't found something :(")
	}

	if err := scanner.Err(); err != nil {
		// Handle the error
		fmt.Println("[-] Scanner error")
		fmt.Println(err)
	}
}
