# FaceMash

This project is used to check infos on a person
in an automated way through a go script!
The name of the project is directly 
linked to the first real "social network" 
Zuckerberg used to create at Harvard in october 2003.

This name is only used for ironic purposes, I hate the effort that
Zuck and his Meta company put into their products.
(The mere fact that this leak exists and was published in 2021, 
even though Facebook assured users that there was a patch in 2019,
is a reminder of the danger of using such tools)

This project has for sole purpose to be used by people who want to check 
the informations the leak has compromised on them and OSINT workers 
(other cybersec legal activites) to do their jobs. 
This script should **NEVER, EVER** be used for or to end with 
an illegal purposes like phishing, doxxing etc...


## What's in the project ?
Go Scripts (different build) + db of Facebook leak (Only the French version)

(If you want to modify the script to check with another country, 
You can download the db for another country [there](https://github.com/attakercyebr/Facebook-Data-Leak)
You can follow the instructions there no big deal, 
just remember to combine the files you'll get 
at the end of the download (after the decompressing part),
or modify the script to check multiple files 

## What's in this leak ? 
You should have a look at this article right [here](https://www.businessinsider.com/stolen-data-of-533-million-facebook-users-leaked-online-2021-4?r=US&IR=T)

## DB File type

The file is in a zip format to allow you to clone it faster (compared to its raw .txt format), you should consider using a command like this: `unzip archive.zip` on Linux or `tar -xf archive.zip` on Windows (if your build is the 17063 version or later). If there is a problem just use the GUI to unzip.

## Build ENV
env GOOS=target-OS GOARCH=target-architecture go build package-import-path

GOOS - Target Operating System          GOARCH - Target Platform
android                                 arm
darwin                                  386
darwin                                  amd64
darwin                                  arm
darwin                                  arm64
dragonfly                               amd64
freebsd                                 386
freebsd                                 amd64
freebsd                                 arm
linux                                   386
linux                                   amd64
linux                                   arm
linux                                   arm64
linux                                   ppc64
linux                                   ppc64le
linux                                   mips
linux                                   mipsle
linux                                   mips64
linux                                   mips64le
netbsd                                  386
netbsd                                  amd64
netbsd                                  arm
openbsd                                 386
openbsd                                 amd64
openbsd                                 arm
plan9                                   386
plan9                                   amd64
solaris                                 amd64
windows                                 386
windows                                 amd64

## Script Attribute

-> Depending on your machine,
-> You might need to do: `chmod +x script`
-> to make the script executable 

## Script Usage

-> For email, company etc search like this: `./FaceMash string`

-> Search a number? : `./FaceMash 33612345678` 
-> International code number here (France) is 33, 
-> so if your number start with 07, 
-> it's going to be 337, same for 06 -> 336)

-> Search a  name? : `./FaceMash Firstname,Lastname`
